## [1.2.3](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/compare/v1.2.2...v1.2.3) (2020-08-18)


### Bug Fixes

* use CI_COMMIT_REF_NAME for version ([bb1cf00](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/bb1cf00a8185e6a870389651719c4a8c84ea0dbb))

## [1.2.2](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/compare/v1.2.1...v1.2.2) (2020-08-18)


### Bug Fixes

* update to bionic-20200713 ([88e3dd3](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/88e3dd399bc866309a7a506af3fe3f0b74549f86))

## [1.2.1](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/compare/v1.2.0...v1.2.1) (2020-08-18)


### Bug Fixes

* do not use slug for version ([7f147b4](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/7f147b49c4bae8b6795715feeb4063275993e1b7))

# [1.2.0](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/compare/v1.1.0...v1.2.0) (2020-08-07)


### Features

* add rule to control when it runs ([8a74499](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/8a74499fc38a82a00efd846e67b053abdf42dabd))

# [1.1.0](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/compare/v1.0.0...v1.1.0) (2020-07-30)


### Bug Fixes

* update to use raw markdown source ([410846e](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/410846e21d30d89de049fc7b6536a5d9ab38d882))


### Features

* use envsubst to update config file ([8fdfe4d](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/8fdfe4dedaf5df68a3d9193444192f8ea0139db9))

# 1.0.0 (2020-07-30)


### Bug Fixes

* download from GitHub ([b68b5f3](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/b68b5f34362c5806df83cc9384916f561a9e06d9))


### Features

* add pipeline to share ([8cdc1cf](https://gitlab.com/jeffcook/pandoc-wkhtmltopdf/commit/8cdc1cf32b85a806c0f0fb6e572929153eaa850f))
