#! /bin/bash

PANDOC_TMP=${PANDOC_TMP:-./pandoc.tmp}
mkdir -p $PANDOC_TMP

PANDOC_TMP_CONFIG=$PANDOC_TMP/defaults
mkdir -p $PANDOC_TMP_CONFIG

source $(dirname ${BASH_SOURCE[0]})/lib/ci_metadata.sh
source $(dirname ${BASH_SOURCE[0]})/lib/docctl.sh

ci_metadata
docctl

CONFIG_DIR=$(dirname ${BASH_SOURCE[0]})/config/
source $(dirname ${BASH_SOURCE[0]})/lib/defaults.sh

mv .pandoc.yaml .pandoc.yaml.tmp
cat .pandoc.yaml.tmp | envsubst > .pandoc.yaml

pandoc $PANDOC_DEFAULTS $@
