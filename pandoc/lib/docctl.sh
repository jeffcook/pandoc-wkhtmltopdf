#! /bin/bash

export DOC_CTL_SOURCE=${DOC_CTL_SOURCE:-./.document_control.md}
export DOC_CTL_CONFIG=${DOC_CTL_CONFIG:-$(dirname ${BASH_SOURCE[0]})/docctl.yaml}

docctl() {
  echo; echo Creating Document Control temp $DOC_CTL_SOURCE; echo
  echo DOC_CTL_SOURCE=$DOC_CTL_SOURCE
  echo DOC_CTL_CONFIG=$DOC_CTL_CONFIG
  [[ -f $DOC_CTL_CONFIG ]] || echo "Config file doesn't exist"
  [[ -f $DOC_CTL_CONFIG ]] || exit 1

  PANDOC_TMP_DOC_CTL_TEMP=$PANDOC_TMP/docctl.html

  # Ensure the config file exists and empty
  echo --- > $PANDOC_TMP_CONFIG/docctl.yaml

  [[ -f $DOC_CTL_SOURCE ]] && pandoc --defaults=$DOC_CTL_CONFIG --output=$PANDOC_TMP_DOC_CTL_TEMP $DOC_CTL_SOURCE
  [[ -f $DOC_CTL_SOURCE ]] && echo "include-before-body: ['${PANDOC_TMP_DOC_CTL_TEMP}']" >> $PANDOC_TMP_CONFIG/docctl.yaml

  echo
}
