#! /bin/bash

ci_metadata() {
  OUTPUT=$PANDOC_TMP/ci_metadata.yaml

  echo;echo Creating CI Metatdata File $OUTPUT ; echo

  echo --- > $OUTPUT
  echo date: $(date) >> $OUTPUT
  [[ "$CI_COMMIT_REF_NAME" ]] && echo version: $CI_COMMIT_REF_SLUG >> $OUTPUT
}
