
PANDOC_DEFAULTS=
for f in $CONFIG_DIR/*.yaml
do
  PANDOC_DEFAULTS="${PANDOC_DEFAULTS} --defaults=$f"
done
for f in $PANDOC_TMP_CONFIG/*.yaml
do
  PANDOC_DEFAULTS="${PANDOC_DEFAULTS} --defaults=$f"
done

PANDOC_DEFAULTS="${PANDOC_DEFAULTS} --defaults=.pandoc.yaml"

echo PANDOC_DEFAULTS=$PANDOC_DEFAULTS
