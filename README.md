# Pandoc with wkhtmltopdf

A Docker image to run Pandoc and wkhtmltopdf.

- <https://pandoc.org/MANUAL.html>
- <https://wkhtmltopdf.org/>

## Usage

Run without any additional options.

```bash
docker run --rm -v $PWD:/data registry.gitlab.com/jeffcook/pandoc-wkhtmltopdf
```

Run with additional options.

```bash
docker run --rm -v $PWD:/data registry.gitlab.com/jeffcook/pandoc-wkhtmltopdf /pandoc/pandoc.sh [pandoc options]
```

## Renovate Bot

Testing Renovate Bot.

<https://app.renovatebot.com/dashboard>
