FROM ubuntu:bionic-20200713

WORKDIR /tmp

RUN apt-get update && \
    apt-get install -y wget gettext-base && \
    wget https://github.com/jgm/pandoc/releases/download/2.9.1/pandoc-2.9.1-1-amd64.deb && \
    apt-get install -y -f ./pandoc-*-amd64.deb && \
    wget https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.bionic_amd64.deb && \
    apt-get install -y -f ./wkhtmltox_*.bionic_amd64.deb && \
    rm ./*.deb && \
    apt-get remove -y wget && \
    apt-get clean
    # rm -rf /var/lib/apt/lists/*

WORKDIR /data
COPY pandoc/ /pandoc/
CMD ["/pandoc/pandoc.sh"]
ENV PATH="/pandoc:${PATH}"

RUN useradd -ms /bin/bash pandoc
USER pandoc
